package main

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Entry struct {
	Id    string
	Value int
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func disconnectClient(client *mongo.Client) {
	err := client.Disconnect(context.TODO())
	checkErr(err)
	fmt.Println("Connection closed!")
}

func generateEntries(num int) []interface{} {
	entries := []interface{}{} //needs to be of type interface{}
	for i := 1; i <= num; i++ {
		id := uuid.New()
		entry := Entry{id.String(), i}
		entries = append(entries, entry)
	}
	return entries
}

func main() {

	var cred options.Credential

	cred.Username = "root"
	cred.Password = "root"

	//Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:28017").SetAuth(cred)

	//(Optional) With context
	//ctx, _ := context.WithTimeout(context.Background(),10*time.Second)
	//contextErr := client.Connect(ctx)

	//Connect
	myClient, err := mongo.Connect(context.TODO(), clientOptions)
	defer disconnectClient(myClient)

	checkErr(err)

	//Check the connection
	connectionErr := myClient.Ping(context.TODO(), nil)

	checkErr(connectionErr)

	fmt.Println("Connected!")

	//empty collection
	collection := myClient.Database("test").Collection("entries")

	//entries
	//entries := generateEntries(3)

	/*INSERT ONE*/
	//for _, e := range entries {
	//	insertResult, err := collection.InsertOne(context.TODO(),e)
	//	checkErr(err)
	//	fmt.Println("Inserted a single document: ", insertResult.InsertedID)
	//}

	/*INSERT MULTIPLE*/
	//multipleEs := generateEntries(2)
	//insertMultipleResult, insertErr := collection.InsertMany(context.TODO(),multipleEs)
	//checkErr(insertErr)
	//fmt.Println("Inserted multiple documents: ", insertMultipleResult.InsertedIDs)

	/*UPDATE*/
	filter := bson.D{{"id", "31afaafe-8abc-4229-b925-34173078a088"}}

	//update := bson.D{
	//	{"$inc", bson.D{{"value",5}}},
	//}

	//updateResult, updateErr := collection.UpdateOne(context.TODO(),filter,update)
	//checkErr(updateErr)
	//fmt.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)

	/*FIND ONE*/
	var result Entry
	findErr := collection.FindOne(context.TODO(), filter).Decode(&result)
	checkErr(findErr)
	fmt.Printf("Found a single document: %+v\n", result)

	/*FIND MANY*/
	//Additional options
	findOptions := options.Find()
	findOptions.SetLimit(2) //only two documents

	//array to store results
	var entriesArr []*Entry
	//bson.D{{}} matches all documents
	cursor, findManyErr := collection.Find(context.TODO(), bson.D{{}}, findOptions)

	//closure
	defer func(cursor *mongo.Cursor, ctx context.Context) {
		err := cursor.Close(ctx)
		if err != nil {
			log.Fatal(err)
		}
	}(cursor, context.TODO())

	checkErr(findManyErr)

	//iterating through cursor allows us to decode documents one at a time
	for cursor.Next(context.TODO()) {
		var elem Entry
		curErr := cursor.Decode(&elem)
		checkErr(curErr)

		entriesArr = append(entriesArr, &elem)
	}

	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Found multiple documents (array of pointers): %+v\n", entriesArr)

	for _, d := range entriesArr {
		fmt.Println(*d)
	}

}
