package tests

import (
	"fmt"
	"littleBookGo/refresh/norbis/01/primeFactor"
	"testing"
)

func TestPrimeFactor(t *testing.T) {
	assertion := `[23]`
	result := fmt.Sprintf("%v", primeFactor.GetPrimeFactor(23))
	if result != assertion {
		t.Error(23)
	}
	assertion = `[2 2 3]`
	result = fmt.Sprintf("%v", primeFactor.GetPrimeFactor(12))
	if result != assertion {
		t.Error(12)
	}
	assertion = `[2 2 2 3 3 5]`
	result = fmt.Sprintf("%v", primeFactor.GetPrimeFactor(360))
	if result != assertion {
		t.Error(360)
	}
}
