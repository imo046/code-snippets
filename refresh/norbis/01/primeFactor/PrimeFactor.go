package primeFactor

// GetPrimeFactor named return
func GetPrimeFactor(n int) (pfs []int) {
	//Get number of 2s that divide n
	for n%2 == 0 {
		pfs = append(pfs, 2)
		n = n / 2
	}

	//n is odd by this point, can skip check
	for i := 3; i*i <= n; i += 2 {
		//while i divides n, append i and divide n
		for n%i == 0 {
			pfs = append(pfs, i)
			n = n / i
		}
	}

	if n > 2 {
		pfs = append(pfs, n)
	}
	return pfs
}
