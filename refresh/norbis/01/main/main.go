package main

import (
	"fmt"
	"littleBookGo/refresh/norbis/01/collatzSeq"
	"littleBookGo/refresh/norbis/01/letterCount"
)

func GetRange(n int) []int {
	myRange := make([]int, n-1)
	for i := range myRange {
		myRange[i] = i + 1
	}
	return myRange
}

func GetNat(l []int) []int {
	nLst := make([]int, 0)
	for _, n := range l {
		if n%3 == 0 || n%5 == 0 {
			nLst = append(nLst, n)
		}
	}
	return nLst
}

func GetSum(n []int, i int, currentSum int) int {
	//termination condition
	if i >= len(n) {
		return currentSum
	}
	currentSum += n[i]
	return GetSum(n, i+1, currentSum)

}

func GetFibonacci(n []int, limit int, iterable int, sum int) []int {
	sum += n[iterable]
	if sum >= limit {
		return n
	}
	n = append(n, sum)
	return GetFibonacci(n, limit, iterable+1, sum)

}

func main() {
	nList := GetRange(1000)
	natList := GetNat(nList)
	fmt.Println(GetSum(natList, 0, 0))
	fmt.Println(GetFibonacci([]int{1, 2}, 4000000, 0, 2))
	fList := GetFibonacci([]int{1, 2}, 4000000, 0, 2)
	filter := func(l []int) []int {
		newList := make([]int, 0)
		for _, n := range l {
			if n%2 == 0 {
				newList = append(newList, n)
			}
		}
		return newList
	}
	println(GetSum(filter(fList), 0, 0))
	//fmt.Println(primeFactor.GetPrimeFactor(600851475143))
	seq := make([]int, 0)
	r := collatzSeq.GetCollatzSeq(18, seq)
	fmt.Println(r)
	fmt.Println(len(r))
	//start := time.Now()
	//longestVal := collatzSeq.GetLongestSeq(999999, 0)
	//elapsed := time.Since(start)
	//fmt.Println(longestVal)
	//log.Printf("Function took %s", elapsed)
	letterCount.GetLetterCount(209)
}
