package collatzSeq

func GetCollatzSeq(n int, seq []int) []int {
	seq = append(seq, n)
	if n == 1 {
		return seq
	}
	switch n % 2 {
	case 0:
		n = n / 2

	case 1:
		n = (3 * n) + 1
	}
	return GetCollatzSeq(n, seq)
}

func GetLongestSeq(n int, maxVal int) int {
	if n <= 1 {
		return maxVal
	}
	l := len(GetCollatzSeq(n, []int{}))
	if maxVal < l {
		maxVal = l
	}
	return GetLongestSeq(n-1, maxVal)
}
