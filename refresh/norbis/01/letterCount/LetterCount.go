package letterCount

import (
	"fmt"
	"strings"
)

var NUMBERSTOLETTERS = map[int]string{
	1: "en", 11: "elleve", 30: "tretti",
	2: "to", 12: "tolv", 40: "forti",
	3: "tre", 13: "tretten", 50: "femti",
	4: "fire", 14: "fjortten", 60: "seksti",
	5: "fem", 15: "femten", 70: "sytti",
	6: "seks", 16: "seksten", 80: "otti",
	7: "sju", 17: "sytten", 90: "nitti",
	8: "åtte", 18: "otten", 100: "hundre",
	9: "ni", 19: "nitten", 1000: "tusen",
	10: "ti", 20: "tjue",
}

func GetLetterCount(n int) (count int) {
	hundreds := n / 100
	tens := (n - hundreds*100) / 10
	nums := n - (hundreds*100 + tens*10)
	var a, b, c string
	if nums > 0 {
		c = NUMBERSTOLETTERS[nums]
	}
	if tens > 0 {
		if tens*10+nums > 19 {
			b = NUMBERSTOLETTERS[tens*10]
		} else {
			b = NUMBERSTOLETTERS[tens*10+nums]
			c = ""
		}
	}
	if hundreds > 0 {
		if hundreds >= 2 {
			a = NUMBERSTOLETTERS[hundreds] + " " + "hundre"
		} else {
			a = "hundre"
		}
	}
	var res []string
	for _, n := range []string{a, b, c} {
		if len(n) > 0 {
			res = append(res, n)
		}
	}
	letterNums := strings.TrimSpace(strings.Join(res, " "))
	fmt.Println(letterNums)
	count = len(letterNums)
	return
}
