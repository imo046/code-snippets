package share

import "fmt"

const Version = "1.0"

func Log(m string) {
	fmt.Println("[LOG]: " + m)
}
