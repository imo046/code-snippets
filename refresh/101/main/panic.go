package main

import (
	"fmt"
	"log"
	"os"
	"runtime/debug"
)

func ErrorHandler() {
	if r := recover(); r != nil {
		//fmt.Println("Recovered", r)
		log.Println(r, string(debug.Stack()))
	}
}
func DivideWithErr(n int, d int) float32 {
	defer ErrorHandler()
	if d == 0 {
		panic("Can't divide by 0")
	}
	return float32(n / d)
}

func Run(n int, d int) {
	f, err := os.OpenFile("./logs", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	defer f.Close()
	if err != nil {
		log.Println(err)
	}
	log.SetOutput(f)
	no := DivideWithErr(n, d)
	fmt.Println(no)
}
