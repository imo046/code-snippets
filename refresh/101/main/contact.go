package main

import "fmt"

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func AddContact(m map[string]string) {
	fmt.Println("Contact: ")
	var contact string
	var no string

	_, contactErr := fmt.Scan(&contact, &no)
	CheckErr(contactErr)
	if _, ok := m[contact]; ok {
		fmt.Println("Contact, exists, overriding")
	}
	m[contact] = no
	fmt.Println("Saved")
}

func FillInContacts() map[string]string {
	var command string
	contacts := make(map[string]string)
	fmt.Println("Start..")
	for {
		fmt.Println("Enter command: ")
		_, enterErr := fmt.Scan(&command)
		CheckErr(enterErr)
		if command == "add" {
			AddContact(contacts)
			continue
		}
		if command == "list" {
			for k, v := range contacts {
				fmt.Println(k, v)
			}
			continue

		}
		if command == "lookup" {
			fmt.Println("Enter name")
			var name string
			_, lookupErr := fmt.Scan(&name)
			CheckErr(lookupErr)
			if n, ok := contacts[name]; !ok {
				fmt.Println("Contact not found!")
				AddContact(contacts)
			} else {
				fmt.Println(name, n)
			}
			continue
		}
		if command == "q" {
			break
		} else {
			fmt.Println("Unknown command: ", command)
			continue
		}
	}
	fmt.Println("Bye")
	return contacts

}
