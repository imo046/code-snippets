package main

import (
	"fmt"
)

type Player struct {
	id   int
	name string
}

func StartGame(numOfP int) {
	var players []Player
	for i := 0; i < numOfP; i++ {
		var pName string
		fmt.Println("Enter name")
		fmt.Scan(&pName)
		players = append(players, Player{i, pName})
	}
	fmt.Println("Players are")
	fmt.Printf("%v", players)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	//fmt.Println("N of players: ")
	//var n string
	//fmt.Scan(&n)
	//N, err := strconv.Atoi(n)
	//checkErr(err)
	//StartGame(N)
	//Run(10, 0)
	FillInContacts()

}
