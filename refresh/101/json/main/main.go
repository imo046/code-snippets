package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

type Config struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Repository  string `json:"repository"`
	Versioned   bool   `json:"versioned"`
	Header      string `json:"header"`
}

type Configs struct {
	Configs []Config `json:"configs"`
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func MarshallData(d *Config) {
	myConfig, err := json.Marshal(d)
	checkErr(err)
	fmt.Println(string(myConfig))
}

func main() {
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(path)
	file, _ := ioutil.ReadFile(path + "/refresh/101/json/main/test.json")
	data := Configs{}
	_ = json.Unmarshal(file, &data)
	for i := 0; i < len(data.Configs); i++ {
		fmt.Println(data.Configs[i].Name)
	}
	myP := &data.Configs[0]
	MarshallData(myP)
}
