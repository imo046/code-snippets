package main

import (
	"fmt"
	"net/http"
)

type Config struct {
	Id   string
	Name string
}

func test(rw http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(rw, "test\n") //takes a writer, any IO stream
}

func GetJson(rw http.ResponseWriter, request *http.Request) {
	rw.Header().Set("Content-Type", "application/json")

}

func main() {
	//Define routes
	http.HandleFunc("/test", test)
	//http.ListenAndServe(":8090", nil)

}
