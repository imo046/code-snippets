package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"littleBookGo/getGAZ/getJsonSource"
	"reflect"
	"strings"
)

func getFile(p string) []byte {
	data, err := ioutil.ReadFile(p)
	if err != nil {
		fmt.Print(err)
		return nil
	} else {
		return data
	}
}

type gazIDs struct {
	gazIDList []interface{}
}

func (g *gazIDs) addIds(countries map[string]interface{}) {
	for _, v := range countries {
		processCountries(v, &g.gazIDList)
	}
}

func checkDuplicates(lst []interface{}) []interface{} {
	var keys map[string]bool
	var d []interface{}
	for _, l := range lst {
		if _, val := keys[l.(string)]; val {
			keys[l.(string)] = true
			d = append(d, l)
		}
	}
	return d
}

func processCountries(v interface{}, p *[]interface{}) {

	switch reflect.TypeOf(v).Kind() {

	case reflect.Array, reflect.Slice:
		for _, i := range v.([]interface{}) {
			processCountries(i, p)
		}
	case reflect.Map:
		for _, v := range v.(map[string]interface{}) {
			processCountries(v, p)
		}

	case reflect.String:
		if strings.Contains(v.(string), "GAZ") {
			*p = append(*p, v)
		}
	}

}

func walk(v reflect.Value) []interface{} {
	var countries []interface{}
	//fmt.Printf("Visiting %v\n", v)
	for v.Kind() == reflect.Ptr || v.Kind() == reflect.Interface {
		v = v.Elem()
	}
	switch v.Kind() {
	case reflect.Array, reflect.Slice:
		for i := 0; i < v.Len(); i++ {
			walk(v.Index(i))
		}
	case reflect.Map:
		for _, k := range v.MapKeys() {
			walk(v.MapIndex(k))
		}
	case reflect.String:
		countries = append(countries, reflect.ValueOf(v.String()))
	}
	return countries
}

type AttributeList struct {
	_id          string
	_official    string
	_iri         string
	World_region string
	Val          string
	_type        string
	Synonyms     string
	_key         string
	Country      string
}

type Relationships struct {
}

type Meta struct {
}

type Links struct {
	Self string
}

type Data struct {
	Type          string `json:"type"`
	Id            string
	Attributes    AttributeList
	Relationships Relationships
}

type GazSource struct {
	Data   Data
	Links  Links
	Errors []string
	Meta   Meta
}

func modifyCountries(s *string, m map[interface{}]string) {

	for k, v := range m {
		*s = strings.Replace(*s, k.(string), v, 1)
	}

}

func writeToPath(p string, b []byte) error {
	err := ioutil.WriteFile(p, b, 0644)
	return err
}

func main() {
	foo := &GazSource{}
	url := "http://localhost:9000/rest/v1/-/ciris/"
	//getJsonSource.GetSource(url,foo)
	//fmt.Println(foo.Data.Attributes.Val)
	test := getFile("/media/igor/65d5f130-cee4-4463-b961-b674941218ee/igor/go/src/littleBookGo/getGAZ/countries-10m.json")
	var ids []interface{}
	g := &gazIDs{ids}
	var result map[string]interface{}
	json.Unmarshal(test, &result)
	countries := result["objects"].(map[string]interface{})
	g.addIds(countries)

	gazMap := make(map[interface{}]string)
	for _, v := range g.gazIDList {
		getJsonSource.GetSource(url+v.(string), foo)
		gazMap[v] = v.(string) + "--" + foo.Data.Attributes.Val
	}
	s := string(test)
	modifyCountries(&s, gazMap)
	test2 := []byte(s)
	writeToPath("/media/igor/65d5f130-cee4-4463-b961-b674941218ee/igor/go/src/littleBookGo/getGAZ/out.json", test2)
	//json.Unmarshal(test2, &result)
	//countries = result["objects"].(map[string]interface{})
	//fmt.Println(countries)

}
