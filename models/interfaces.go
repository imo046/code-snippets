package models

import (
	"fmt"
	"math"
)

type Logger interface {
	Log(message string)
}

type Geometry interface {
	area() float64 //methods to implement
	perim() float64
}

type Server struct {
	logger Logger
}

func process(logger Logger) {
	logger.Log("test")
}

type Rect struct {
	Width, Height float64
}

type Circle struct {
	Radius float64
}

func (r *Rect) area() float64 {
	return r.Height * r.Width
}

func (r *Rect) perim() float64 {
	return 2*r.Height + 2*r.Width
}

func (c *Circle) area() float64 {
	return math.Pi * c.Radius * c.Radius
}

func Measure(g Geometry) {
	fmt.Println(g.area())
	fmt.Println(g.perim())
}
