package main

import "fmt"

type Node struct {
	value int
	next  *Node
}

type LinkedList struct {
	head   *Node
	length int
}

func (l *LinkedList) prepend(n *Node) {
	second := l.head
	l.head = n
	l.head.next = second
	l.length += 1
}

func (l LinkedList) printLinkedList() {
	toPrint := l.head
	for l.length != 0 {
		fmt.Printf("%d ", toPrint.value)
		toPrint = toPrint.next
		l.length -= 1
	}
}

func (l *LinkedList) deleteByValue(v int) {
	if l.length < 1 {
		return
	}
	if l.head.value == v {
		l.head = l.head.next
		l.length -= 1
		return
	}
	toDeletePrevious := l.head
	for toDeletePrevious.next.value != v {
		if toDeletePrevious.next.next == nil {
			fmt.Printf("Value %d not in the list!\n", v)
			return
		}
		toDeletePrevious = toDeletePrevious.next
	}
	toDeletePrevious.next = toDeletePrevious.next.next
	l.length -= 1
}

func main() {

	myList := LinkedList{}
	n1 := &Node{10, &Node{}}
	n2 := &Node{20, &Node{}}
	n3 := &Node{30, &Node{}}
	n4 := &Node{40, &Node{}}

	myList.prepend(n1)
	myList.prepend(n2)
	myList.prepend(n3)
	myList.prepend(n4)

	myList.printLinkedList()
}
