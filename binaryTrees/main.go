package main

import "fmt"

// Node
type Node struct {
	value int
	left  *Node
	right *Node
}

// Insert will add a node to the tree, if it is not present already
func (n *Node) Insert(v int) {
	if n.value < v {
		//move right
		if n.right == nil {
			//if right child is empty, make a new node
			n.right = &Node{v, nil, nil}
		} else {
			n.right.Insert(v)
		}
	} else if n.value > v {
		//move left
		if n.left == nil {
			//if right child is empty, make a new node
			n.left = &Node{v, nil, nil}
		} else {
			n.left.Insert(v)
		}
	}

}

// Search will take in a key value and return true if there is a node with that value
func (n *Node) Search(v int) bool {
	if n == nil {
		//if we don't meet a value in a tree, we return false
		return false
	}
	if n.value < v {
		//move right
		n.right.Search(v)

	} else if n.value > v {
		//move left
		n.left.Search(v)
	}
	//if value not smaller or larger means it is a match, we return true
	return true

}

func main() {

	tree := &Node{100, nil, nil}

	list := []int{200, 50, 19, 76, 150, 310, 7, 24, 56, 88, 135, 167, 276, 433}
	for _, v := range list {
		tree.Insert(v)
	}
	fmt.Println(tree)
	fmt.Println(tree.Search(19))
}
