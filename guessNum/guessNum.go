package guessNum

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

func getRand(from int, until int) int {
	seed := time.Now().Unix() //time.now converted to int, used as seed for random value
	rand.Seed(seed)
	target := rand.Intn(until) + from
	return target
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func guessNum() int {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("guess: ")
	input, err := reader.ReadString('\n')
	checkErr(err)
	input = strings.TrimSpace(input)
	answer, err := strconv.Atoi(input)
	checkErr(err)
	return answer
}

func compareWithNum(num int) bool {
	answer := guessNum()
	fmt.Println(num)
	if answer < num {
		fmt.Println("LOW")
		return false
	} else if answer > num {
		fmt.Println("HIGH")
		return false
	}
	return true

}

func Play(times int, from int, until int) {
	num := getRand(from, until)
	for i := times; i > 0; i-- {
		if !compareWithNum(num) {
			fmt.Printf("You have %d tries \n", i)
		} else {
			fmt.Printf("You win, the answer is %d", num)
			return
		}
	}
}
