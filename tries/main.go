package main

import "fmt"

const AlphabetSize = 26

// Trie represent our tree structure and has a pointer to the root node
type Trie struct {
	root *Node
}

// Node represent each node in the trie
type Node struct {
	//Each node will have 26 children according to size of the alphabet
	children [AlphabetSize]*Node
	isEnd    bool
}

// Insert will take in a word and add it to the trie
func (t *Trie) Insert(w string) {
	currentNode := t.root
	for _, v := range w {
		charIndex := v - 'a'                        //index of the char in the alphabet (index of 'b' is 1)
		if currentNode.children[charIndex] == nil { //currentNode has no child with the current index
			currentNode.children[charIndex] = &Node{}
		}
		currentNode = currentNode.children[charIndex] //traverse further to the next node
	}
	currentNode.isEnd = true
}

// Search will take in  a word and return true if this word is present in the trie
func (t *Trie) Search(w string) bool {
	currentNode := t.root
	for _, v := range w {
		charIndex := v - 'a'                        //index of the char in the alphabet (index of 'b' is 1)
		if currentNode.children[charIndex] == nil { //currentNode has no child with the current index
			return false
		}
		currentNode = currentNode.children[charIndex] //traverse further to the next node
	}
	if currentNode.isEnd == true { //if successfully traversed through the nodes meaning each letter exists and the word
		// already in the trie
		return true
	} // even if we have all matches but the last node is not an end means no such word found
	return false
}

// InitTrie
func InitTrie() *Trie {
	return &Trie{&Node{}}
}

func main() {

	myTrie := InitTrie()
	myTrie.Insert("araragi")
	fmt.Println(myTrie.Search("araragi"))
	myTrie.Insert("aragorn")
	myTrie.Insert("oregon")
	myTrie.Insert("eragon")
	myTrie.Insert("argon")
	myTrie.Insert("oregano")
	myTrie.Insert("oreo")
	fmt.Println(myTrie.Search("eragon"))
	fmt.Println(myTrie.Search("oreimo"))
}
