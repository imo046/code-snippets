package main

import "fmt"

type Tree struct {
	Left  *Tree
	Value int
	Right *Tree
}

func (t *Tree) Insert(k int) {
	if t.Value < k { //move right
		if t.Right == nil {
			t.Right = &Tree{nil, k, nil}
		} else {
			t.Right.Insert(k)
		}
	} else if t.Value > k { //move left
		if t.Left == nil {
			t.Left = &Tree{nil, k, nil}
		} else {
			t.Left.Insert(k)
		}
	}

}

func (t *Tree) String() string {
	if t == nil {
		return "()"
	}
	s := ""
	if t.Left != nil {
		s += t.Left.String() + " "
	}
	s += fmt.Sprint(t.Value)
	if t.Right != nil {
		s += " " + t.Right.String()
	}
	return "(" + s + ")"
}

func (t *Tree) Walk(ch chan int) {
	if t == nil {
		return
	}
	t.Left.Walk(ch)
	ch <- t.Value
	//*keys = append(*keys,t.Value)
	t.Right.Walk(ch)
}

func (t *Tree) Search(n int) bool {
	if t == nil {
		return false
	}
	if t.Value < n { //move right
		t.Right.Search(n)
	}
	if t.Value > n { //move left
		t.Left.Search(n)
	}
	return true

}

func Same(t1 *Tree, t2 *Tree) bool {
	ch1 := make(chan int)
	ch2 := make(chan int)
	go t1.Walk(ch1)
	go t2.Walk(ch2)

	for i := 0; i < 10; i++ {
		if <-ch1 != <-ch2 {
			return false
		}
	}
	return true

}

func main() {
	tree1 := &Tree{nil, 100, nil}
	tree2 := &Tree{nil, 100, nil}
	//keys := []int{}
	ch := make(chan int)
	list := []int{200, 50, 19, 76, 150, 310, 7, 24, 56, 88, 135, 167, 276, 433}
	for _, v := range list {
		tree1.Insert(v)
	}
	for _, v := range list {
		tree2.Insert(v)
	}
	go func(ch chan int) {
		tree1.Walk(ch)
		//close(ch)
	}(ch)
	//for v := range ch {
	//	fmt.Println(v)
	//}
	for i := 0; i < 10; i++ {
		fmt.Println(<-ch)
	}
	fmt.Println(Same(tree1, tree2))

}
