package main

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

func ReadFile(p string) []byte {
	data, err := ioutil.ReadFile(p)
	if err != nil {
		fmt.Print(err)
		return nil
	} else {
		return data
	}
}

func GetFile(p string) (*os.File, error) {
	return os.Open(p)
}

func ScanFile(file *os.File) {
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func ReadDir(p string) {
	fmt.Println(p)
	files, err := ioutil.ReadDir(p)
	if err != nil {
		defer func() {
			if r := recover(); r != nil {
				fmt.Println(r) //Print panic message if panic
			}
		}()
		panic(err)
	}
	for _, file := range files {
		filePath := filepath.Join(p, file.Name())
		if file.IsDir() {
			ReadDir(filePath)
		} else {
			fmt.Println(filePath)
		}
	}
}
func reportPanic() {
	if p := recover(); p != nil {
		err, ok := p.(error)
		if ok {
			fmt.Println(err.Error())
		} else {
			panic(err) //resume panic if not expected
		}
	} else {
		return
	}
}

func callPanic() {
	err := errors.New("Panic error")
	panic(err)
}

func main() {
	path := "/home/igor/Projects/marrefModifyTsv"
	ReadDir(path)
	defer reportPanic()
	callPanic()
}
