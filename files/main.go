package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func writeToFile(p string, line interface{}) {
	options := os.O_WRONLY | os.O_APPEND | os.O_CREATE
	f, err := os.OpenFile(p, options, os.FileMode(0600))
	check(err)
	_, err = fmt.Fprintln(f, line)
	check(err)
	defer func() {
		err := f.Close()
		check(err)
	}()
}

func readFromFile(p string) {
	f, err := os.Open(p)
	check(err)
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {

	}
}

func main() {

	curr_path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	for _, v := range []string{"one", "two", "three"} {
		writeToFile(curr_path+"/files/test.txt", v)
	}

}
