package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	args := os.Args[1:]
	s := 0.0
	p := &s
	for _, n := range args {
		if val, err := strconv.ParseFloat(n, 64); err == nil {
			*p += val
		}
	}
	fmt.Println(s)
}
