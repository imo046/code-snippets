package main

import "fmt"

func main() {
	//Variables
	//Init zero values
	var a int
	var b bool
	aa := int32(10)
	fmt.Printf("a \t %T [%v] \n", a, a)
	fmt.Printf("b \t %T [%v] \n", b, b)
	fmt.Printf("aa \t %T [%v] \n", aa, aa)

	//Structs
	type myStruct struct {
		flag    bool
		counter int
	}

}
