package gadget

import "log"

type Device interface {
	Play(sound string)
	Stop()
}

type Player struct {
	Charge string
}
type Recorder struct {
	Mic int
}

func (p *Player) Play(song string) {
	log.Println("Playing", song)
}

func (p *Player) Stop() {
	log.Println("Stopped")
}

func (p *Recorder) Play(song string) {
	log.Println("Playing", song)
}

func (p *Recorder) Record() {
	log.Println("Recording")
}

func (p *Recorder) Stop() {
	log.Println("Stopped")
}
