package main

import (
	"fmt"
	"littleBookGo/interfaces/gadget"
	"strconv"
)

type Temp int

func (t Temp) String() string {
	return strconv.Itoa(int(t)) + " C"
}

func play(d gadget.Device, s string) {
	d.Play(s)
}

type MyError string

func (e MyError) Error() string {
	return string(e)
}

func main() {
	var value gadget.Device
	var err error
	err = MyError("My custom error")
	fmt.Println(err)
	var x fmt.Stringer //Interface, since Temp implemented string method, it is implementing this interface
	x = Temp(12)
	fmt.Printf("%v %T \n", x, x)
	value = &gadget.Player{"100%"}
	recorder := &gadget.Recorder{}
	player, ok := value.(*gadget.Player)
	if ok {
		play(player, "play")
	}
	play(value, "foo")
	play(recorder, "bar")
}
