package main

import (
	"fmt"
	"littleBookGo/generics/src"
)

func main() {
	// Initialize a map for the integer values
	ints := map[string]int64{
		"first":  34,
		"second": 12,
	}

	// Initialize a map for the float values
	floats := map[string]float64{
		"first":  35.98,
		"second": 26.99,
	}

	fmt.Printf("Non-Generic Sums: %v and %v\n",
		src.SumInts(ints),
		src.SumFloats(floats))

	fmt.Printf("Generic Sums, type parameters inferred: %v and %v\n",
		src.SumIntsOrFloats(ints),
		src.SumIntsOrFloats(floats))

}
