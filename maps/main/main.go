package main

import (
	"fmt"
	"strconv"
)

func add(x, y int) int {
	return x + y
}

func sub(x, y int) int {
	return x - y
}

type MyFunc func(int, int) int

var OperMap = map[string]MyFunc{
	"+": add,
	"-": sub,
}

func handleErr(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

func main() {
	inputs := [][]string{
		[]string{"2", "+", "3"},
		[]string{"3", "-", "2"},
	}
	for _, v := range inputs {
		var1, err1 := strconv.Atoi(v[0])
		handleErr(err1)

		op := v[1]
		opF, ok := OperMap[op]
		if !ok {
			fmt.Printf("Wrong operator %s", op)
			continue
		}

		var2, err2 := strconv.Atoi(v[2])
		handleErr(err2)

		res := opF(var1, var2)

		fmt.Println(res)
	}
}
