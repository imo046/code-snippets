package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func getErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func responseSize(url string) int {
	response, err := http.Get(url)
	getErr(err)
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	getErr(err)
	return len(body)
}

func responseSizeCh(ch chan URLInfo, url string) {
	response, err := http.Get(url)
	getErr(err)
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	getErr(err)
	ch <- URLInfo{url, len(body)}
}

func GetHttpContent(url string) {
	response, err := http.Get(url)
	getErr(err)
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	getErr(err)
	fmt.Println(string(body))
}

func chIn(ch chan URLInfo, value URLInfo) {
	ch <- value
}

func chOut(ch chan URLInfo) URLInfo {
	return <-ch
}

type URLInfo struct {
	url    string
	length int
}

func (i URLInfo) GetInfo() {
	fmt.Printf("url: %s\nlength: %d\n", i.url, i.length)
}

func addToChAndClose(ch chan URLInfo, urlList []string) {
	for _, url := range urlList {
		responseSizeCh(ch, url)
	}
	close(ch)
}

func main() {
	url := "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/009/831/655/GCA_009831655.1_ASM983165v1/"
	url1 := "https://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt"
	url2 := "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/905/175/375/GCF_905175375.1_Chryseobacterium_CECT9390/"
	//GetHttpContent(url)
	ch := make(chan URLInfo)
	urlLst := []string{url, url2, url1}
	var urlInfoLst []URLInfo
	for _, t := range urlLst {
		go func(task string) {
			responseSizeCh(ch, task)
		}(t)
	}
	//close(ch)
	for i := 0; i < len(urlLst); i++ {
		urlInfoLst = append(urlInfoLst, <-ch)
	}
	//for _,v := range urlInfoLst {
	//	v.GetInfo()
	//}

	/***
	use of range
	***/
	go addToChAndClose(ch, urlLst)
	for v := range ch {
		v.GetInfo()
		//_,ok := <-ch
		//fmt.Println(ok)
	}

}
