package main

import (
	"fmt"
	"littleBookGo/titbits"
	"math/rand"
	"time"
)

func main() {

	testVars := []string{"one", "two", "three"}
	ch := make(chan string, 3)
	chWorkers := make(chan int, 10)

	for i := 0; i < 4; i++ {
		go func(id int) {
			worker := titbits.WorkerStruct{Id: id}
			worker.Process(chWorkers)
		}(i)

	}

	//for {
	//	select {
	//	case chWorkers <- rand.Int():
	//		fmt.Println("processing")
	//	case <-time.After(time.Millisecond * 100):
	//		fmt.Println("time out")
	//		//default:
	//		//	fmt.Println("dropped")
	//	}
	//	time.Sleep(time.Millisecond * 50)
	//}

	for _, v := range testVars {
		go func(s string) {
			ch <- s
			fmt.Println("Writing")
		}(v)
	}
	time.Sleep(time.Millisecond * 100)
	for i := 0; i < 3; i++ {
		go titbits.Worker(ch)
		time.Sleep(time.Millisecond * 100)
	}

	switch n := rand.Intn(10); {
	case n == 0:
		fmt.Println("Low")
	case n == 6:
		fmt.Println("High")
	default:
		fmt.Println("Good")
	}

	myInt := 4
	MySum(&myInt, 1, 2, 3, 5)
	fmt.Println(myInt)

}

func MySum(base *int, vals ...int) {
	for _, v := range vals {
		*base += v
	}
}
