package titbits

import (
	"fmt"
	"sync"
	"time"
)

type WorkerStruct struct {
	Id int
}

func (w *WorkerStruct) Process(c <-chan int) {
	for {
		data := <-c
		fmt.Printf("worker %d got %d \n", w.Id, data)
		time.Sleep(time.Millisecond * 500) //make it sleep after receiving data to block execution
	}
}

var (
	counter = 0
	lock    sync.Mutex
)

func Incr() {
	lock.Lock()
	defer lock.Unlock()
	counter++
	fmt.Println(counter)
}

func Worker(ch <-chan string) {
	res, ok := <-ch
	if ok {
		fmt.Println(res)
	} else {
		fmt.Println("Channel closed!")
	}
}
