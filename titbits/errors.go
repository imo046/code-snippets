package titbits

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)

func checkErr(e error) {
	if e != nil {
		fmt.Println("Not a valid number")
		os.Exit(1)
	}
}

type MyError struct{}

func (e *MyError) Error() {

}

func ConvertInt(s string) {
	n, err := strconv.Atoi(s)
	checkErr(err)
	fmt.Println(n)
}

func assertMyT(v interface{}) (interface{}, error) {
	res, ok := v.(int)
	if !ok {
		return nil, errors.New("wrong type")
	}
	return res, nil

}

type AddFunc func(a int, b int) int

type MyAddStruct struct {
	add AddFunc
}

func Process(adder AddFunc) int {
	return adder(1, 2)
}

func TestEmptyInterfaces(a interface{}, b interface{}) interface{} {
	_, error1 := assertMyT(a)
	_, error2 := assertMyT(b)

	if error1 == nil && error2 == nil {
		return a.(int) + b.(int)
	}
	return nil
}
