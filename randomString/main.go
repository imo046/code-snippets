package main

import (
	"fmt"
	"github.com/google/uuid"
	"math/rand"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func GetUUID() {
	for i := 0; i < 2; i++ {
		fmt.Println(uuid.New())
	}
}

func main() {

	fmt.Println(RandStringBytes(8))
	fmt.Println(RandStringBytes(8))

}
