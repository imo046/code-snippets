package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

type Point struct {
	x float64
	y float64
}

type PointInt struct {
	x int
	y int
}

// implements sort.Interface based on middle position of the point
type ByMiddlePoint []Point

func (a ByMiddlePoint) Len() int {
	return len(a)
}

// Less comparison
func (a ByMiddlePoint) Less(i, j int) bool {
	return (a[i].x+a[i].y)/2 < ((a[j].x + a[j].y) / 2)
}
func (a ByMiddlePoint) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

type MySort func(int, int) bool

func GetPointsSorted(arr []PointInt, f MySort) {
	sort.Slice(arr, f)
}

func MultInt(base int) func(int) int {
	return func(i int) int {
		return base * i
	}
}

type MyIntErr struct {
	X int
	Y int
}

func (e *MyIntErr) Error() string {
	return fmt.Sprintf("%d - %d is less than zero!", e.X, e.Y)
}

func CheckErr(v1, v2 int) (err error) {
	if v1-v2 < 0 {
		err = &MyIntErr{v1, v2}
	}
	return err
}

// Naming return values allow us to return them beforehand with the default values
func GetFileHelper(name string) (*os.File, func(), error) {
	file, err := os.Open(name)
	if err != nil {
		return nil, nil, err
	}
	return file, func() {
		file.Close()
	}, nil
}

func HandleError(x, y int) {
	err := CheckErr(x, y)
	//do something
	defer func() {
		if err == nil {
			fmt.Println(x - y)
		} else {
			fmt.Println(err)
		}
	}()
}

func GetFile(path string) {
	var checkErr = func(err error) {
		if err != nil {
			log.Fatalln(err)
		}
	}
	f, closer, err := GetFileHelper(path)
	checkErr(err)
	reader := bufio.NewReader(f)
	defer closer()
	line, err := reader.ReadString('\n')
	checkErr(err)
	fmt.Println(line)
}

func main() {
	points := []Point{
		{3, 3},
		{1.5, 9},
		{2, 7},
	}

	pointsInt := []PointInt{
		{8, 3},
		{2, 9},
		{2, 7},
	}

	sort.Sort(ByMiddlePoint(points))
	fmt.Println(points)

	GetPointsSorted(pointsInt, func(i int, j int) bool {
		return (pointsInt[i].x + pointsInt[i].y) < (pointsInt[j].x + pointsInt[j].y)
	})
	fmt.Println(pointsInt)

	baseTwo := MultInt(2)
	fmt.Println(baseTwo(2))

	HandleError(1, 1)
	HandleError(1, 2)
}
