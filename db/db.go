package db

type Item struct {
	Id    int
	Price float64
}

func CreateItem(id int, price float64) *Item {
	return &Item{
		Id:    id,
		Price: price,
	}
}

func LoadItem(id int) *Item {
	return &Item{
		Price: 9.001,
	}
}
