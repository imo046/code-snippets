package getLostRecs

import (
	"bufio"
	"os"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func GetRecords(p string, out string) {
	file, err := os.Open(p)
	check(err)
	defer file.Close()

	f, err := os.Create(out)
	check(err)
	defer f.Close()

	scanner := bufio.NewScanner(file)
	w := bufio.NewWriter(f)

	for scanner.Scan() {
		data := strings.Split(scanner.Text(), " ")[0] + "\n"
		_, err := w.WriteString(data)
		check(err)
	}
	w.Flush()
	if err := scanner.Err(); err != nil {
		panic(err)
	}
}
