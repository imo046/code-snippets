package main

import (
	"littleBookGo/getLostRecs"
	"os"
)

func main() {
	rootPath, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	filePath := rootPath + "/getLostRecs" + "/lostRecs_2.0.txt"
	outPath := rootPath + "/getLostRecs" + "/2.0.txt"
	getLostRecs.GetRecords(filePath, outPath)
}
