package taskHandler

import (
	"fmt"
	"time"
)

var testInput []func() error

func fin(chIn chan<- error, err error) {
	fmt.Println("Writing!")
	chIn <- err
}

func fout(chOut <-chan error) {
	e, ok := <-chOut
	fmt.Printf("error: %s, status: %v\n", e, ok)
}

func TaskHandler(n int, maxErrs int, tasks []func() error) {
	chErr := make(chan error, n)
	for _, t := range tasks {
		go func(t func() error) { //execute task
			chErr <- t()
			time.Sleep(time.Millisecond * 10)
		}(t)
	}
	//close(chErr)
	//go fout(chErr)
	time.Sleep(1 * time.Second)
	fmt.Printf("len: %d, cap: %d\n", len(chErr), cap(chErr))
	for i := 0; i < n; i++ {
		go fout(chErr)
		//time.Sleep(time.Millisecond * 10)
	}

	fmt.Printf("len: %d, cap: %d\n", len(chErr), cap(chErr))
}
