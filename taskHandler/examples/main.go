package main

import (
	"fmt"
	"littleBookGo/taskHandler"
	"time"
)

type MyError struct{}

func (e *MyError) Error() string {
	return "my error"
}

func main() {

	tests := []func() error{
		func() error {
			time.Sleep(2 * time.Second)
			fmt.Println("Task 1 executes")
			return &MyError{}
		},
		func() error {
			fmt.Println("Task 2 executes")
			return &MyError{}
		},
		func() error {
			time.Sleep(1 * time.Second)
			fmt.Println("Task 3 executes")
			return &MyError{}
		},
	}
	taskHandler.TaskHandler(3, 3, tests)

}
