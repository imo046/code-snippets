package main

import (
	"errors"
	"fmt"
	greeting "littleBookGo/greeting"
	"littleBookGo/greeting/numericUtils/getSum"
	"log"
	"math"
)

type OuterStruct struct {
	InnerStruct
}

// named types
type MyFloat float64
type Number int
type Liters float64
type Gallons float64

func (l Liters) ToGallons() Gallons {
	return Gallons(l * 0.264)
}

type GetFloatVal struct {
	Val float64
}

func (g *GetFloatVal) SetVal(v float64) error {
	if v > 0.0 {
		g.Val = v
		return nil
	} else {
		return errors.New("Wrong value")
	}

}

func (f *GetFloatVal) Round() {
	f.Val = math.Round(f.Val)
}

func (n *Number) Double() {
	*n *= 2
}

func (f *MyFloat) Round() {
	*f *= 2
}

type InnerStruct struct {
	x interface{}
}

func main() {
	test := []int{1, 2, 3, 4}
	fmt.Println(getSum.GetSum(test))
	greeting.Hello()
	myStruct := &OuterStruct{}
	fmt.Println(myStruct.x)
	myFloat := GetFloatVal{10.6}
	n := Number(4)
	n.Double()
	myFloat.Round()
	fmt.Println(myFloat.Val)
	err := myFloat.SetVal(-1)
	if err != nil {
		log.Fatal(err)
	}
	myFloat.SetVal(1.1)
	fmt.Println(myFloat)
}
