package main

import "fmt"

// MaxHeap struct has a slice that holds the array
type MaxHeap struct {
	items []int
}

// maxHeapifyUp will heapify from bottom to top
func (h *MaxHeap) maxHeapifyUp(index int) {
	//swap if the current index larger than parent
	for h.items[parent(index)] < h.items[index] { //while...
		h.swapKeys(parent(index), index)
		index = parent(index)
	}
}

// maxHeapifyDown will heapify from top to bottom
func (h *MaxHeap) maxHeapifyDown(index int) {
	//first get the larger child, compare it to the current index and then swap it
	lastIndex := len(h.items) - 1
	l, r := left(index), right(index)
	childToCompare := 0
	//loop while index has at least one child
	for l <= lastIndex {
		//when left child is the only child
		//when left child is larger
		if l == lastIndex || h.items[l] > h.items[r] {
			childToCompare = l
		} else { //when right child is larger
			childToCompare = r
		}
		//compare array value of current index to a larger child and swap if smaller
		if h.items[index] < h.items[childToCompare] {
			h.swapKeys(childToCompare, index)
			index = childToCompare
			l, r = left(index), right(index)
		} else {
			//if current index larger than the largest child meaning it is at the correct place, we are done
			return
		}
	}
}

// returns parent index
func parent(i int) int {
	//parent index x 2 + 1 = left child index, left child index is always an odd number (2,4...) and right child index is always even
	return (i - 1) / 2
}

// returns left child index
func left(i int) int {
	return i*2 + 1 //odd
}

func (h *MaxHeap) swapKeys(parent, child int) {
	h.items[child], h.items[parent] = h.items[parent], h.items[child]
}

// returns right child index
func right(i int) int {
	return i*2 + 2 //even
}

// Insert adds an element the the heap tree
func (h *MaxHeap) Insert(key int) {
	h.items = append(h.items, key)
	h.maxHeapifyUp(len(h.items) - 1)
}

// Extract returns the largest key, and removes it from the heap
func (h *MaxHeap) Extract() int {
	res := h.items[0]
	l := len(h.items)
	if l == 0 {
		fmt.Println("Heap is empty!")
		return -1
	}
	h.items[0] = h.items[l-1]
	h.items = h.items[:l-1]

	h.maxHeapifyDown(0) //after changing the root, heapify from the top
	return res
}

func main() {
	m := &MaxHeap{}
	buildHeap := []int{10, 20, 30, 5, 7, 9, 11, 13, 15, 17, 18}
	for _, v := range buildHeap {
		m.Insert(v)
		fmt.Println(m)
	}
	for i := 0; i < 5; i++ {
		m.Extract()
		fmt.Println(m)
	}

}
