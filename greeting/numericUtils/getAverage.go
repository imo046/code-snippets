package numericUtils

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func geAverage() {

}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func GetAverageFromSource(p string) float64 {
	var sum float64
	count := 1.0
	file, err := os.Open(p)
	checkErr(err)
	defer file.Close() //defers until surrounding function returns
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		num, err := strconv.ParseFloat(scanner.Text(), 64)
		checkErr(err)
		sum += num
		count++
	}
	return sum / count

}
