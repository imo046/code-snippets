package getSum

func GetSum(a []int) int {
	count := len(a)
	last := a[count-1]
	if count == 1 {
		return last
	}
	return last + GetSum(a[:count-1])
}
