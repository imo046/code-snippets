package main

import "fmt"

type Stack struct {
	items []int
}

type Queue struct {
	items []int
}

func (q *Queue) enqueue(v int) {
	q.items = append(q.items, v)
}

func (q *Queue) dequeue() int {
	l := len(q.items)
	if l < 1 {
		fmt.Println("Queue is empty")
		return 0
	}
	res := q.items[0]
	q.items = q.items[1:]
	return res
}

func (s *Stack) push(v int) {
	s.items = append(s.items, v)
}

// remove value at the end and return it
func (s *Stack) pop() int {
	l := len(s.items)
	if l < 1 {
		fmt.Println("Stack is empty")
		return 0
	}
	res := s.items[l-1]
	s.items = s.items[:l-1]
	return res
}
func main() {

}
