package main

import (
	"bufio"
	"errors"
	"fmt"
	"littleBookGo/models"
	"log"
	"math/rand"
	"os"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"
)

// structures, similar to classes
type Person struct {
	Id   int
	Name string
}

type Employee struct {
	p   *Person
	Job string
}

func constructNewEmployee(p *Person, job string) *Employee {
	return &Employee{
		p:   p,
		Job: job,
	}
}

// constructing an instance of Person and returning pointer to this instance
func constructNewPerson(id int, name string) *Person {
	return &Person{
		Id:   id,
		Name: name,
	}
}

// simple function to modify pointer
func changePointer(p *Person) {
	p.Id += 1
}

func testPointer() int {
	myPointer := &Person{0, "Tom"}
	changePointer(myPointer)
	return myPointer.Id
}

// function with receiver, if we need to modify object parameter, better use function with the object as receiver
func (p *Person) callFromReceiver() {
	p.Name = "John Doe"
}

func (p *Employee) callFromReciever() {
	p.Job = "unemployed"
}

func testReceiver() string {
	p := &Person{1, "Mike"}
	p.callFromReceiver()
	return p.Name
}

func createMySlice(n int) []int {
	s := make([]int, n)
	return s
}

func removeAtIndex(source []int, index int) []int {
	lastIndex := len(source) - 1
	//swap
	source[index], source[lastIndex] = source[lastIndex], source[index]
	return source[:lastIndex]
}

func someSort() []int {
	scores := make([]int, 10)
	for i := 0; i < 10; i++ {
		scores[i] = int(rand.Int31n(1000))
	}
	sort.Ints(scores)

	selected := make([]int, 5)
	copy(selected, scores[:5])
	return selected
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

// declaration of outside variable, declaration of variable to its zero value (use = then to assign value)
var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

// create and assign variable ( = for reassign)
func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// error handling
type customErr interface {
	Error() string
}

func returnMyErr() error {
	return errors.New("My error")
}

// possible to declare and assign two vars (or reassign one if new variable is present)
func readFile(p string) {
	file, err := os.Open(p)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func myConvert(s string) {
	if len(s) < 1 {
		log.Fatal("string too short") //print with exit(1)
	}

	n, err := strconv.Atoi(s)
	if err != nil {
		fmt.Println("not a valid number")
	} else {
		fmt.Println(n)
	}
}

func main() {

	//scores := make([]int,0,5)
	//c := cap(scores)
	//fmt.Println(c)
	//
	//for i:=0; i<25; i++ {
	//	scores = append(scores, i)
	//
	//	if cap(scores) != c {
	//		c = cap(scores)
	//		fmt.Println(c)
	//	}
	//} // answer: 5,10,20,40

	fmt.Println(RandStringRunes(12))
	myPerson := Person{0, "Dail"}
	myPerson.callFromReceiver()
	fmt.Println(myPerson.Name)
	scores1 := make([]int, 0, 5)
	scores2 := make([]int, 5)
	fmt.Println(scores1)
	fmt.Println(scores2)

	scores := []int{1, 2, 3, 4, 5}
	slice := scores[2:4]
	slice[0] = 99
	fmt.Println(reflect.TypeOf(slice))
	fmt.Println(scores)
	haystack := "the spice must flow"
	res := strings.Index(haystack[:], " ")
	fmt.Println(res)
	fmt.Println(removeAtIndex(scores, 2))
	fmt.Println(someSort())

	mymap := make(map[int]*Person)
	mymap[0] = &Person{0, "Mike"}
	personData := mymap[0]
	fmt.Println(personData.Name)

	c := models.Circle{5}
	r := models.Rect{4, 5}
	fmt.Println(c.Radius)
	models.Measure(&r)

}
