package main

import (
	"fmt"
)

// ArrSize is the size of the hashTable array
const ArrSize = 7

// HashTable will hold an array
type HashTable struct {
	array [ArrSize]*Bucket
}

// Our linked list to store elements in
type Bucket struct {
	head *BucketNode
}

// Bucket Node, same as Node in previous linked list, holds key value and address of the next node
type BucketNode struct {
	key  string
	next *BucketNode
}

// Init will create a bucket in each slot of the hash table, return address of the hash table
func Init() *HashTable {
	result := &HashTable{}
	for i := range result.array {
		result.array[i] = &Bucket{}
	}
	return result
}

func hash(key string) int {
	var sumRunes int
	for _, r := range key {
		sumRunes += int(r)
	}
	res := sumRunes % ArrSize
	return res
}

// Insert will take in a key and add it to the hash table array
func (h *HashTable) Insert(key string) {
	index := hash(key) //between 0 and arr size
	h.array[index].insert(key)
}

// Search will take a key and return true if key is stored in the hash table
func (h *HashTable) Search(key string) bool {
	index := hash(key)
	return h.array[index].search(key)
}

// Delete will take in a key and delete it from the hash table if key exists
func (h *HashTable) Delete(key string) {
	index := hash(key)
	h.array[index].delete(key)
}

// insert will take in a key, create a node with the key in the bucket
func (b *Bucket) insert(k string) {
	if !b.search(k) {
		newNode := &BucketNode{key: k, next: &BucketNode{}}
		previous := b.head
		b.head = newNode
		newNode.next = previous
	} else {
		fmt.Printf("%s is already exist", k)
	}

}

// search will take in a key and return true if the bucket has this key
func (b *Bucket) search(k string) bool {
	currentNode := b.head
	for currentNode != nil {
		if currentNode.key == k {
			return true
		}
		//traverse further
		currentNode = currentNode.next
	}
	//no element found
	return false
}

// delete will take in  a key and delete the node from bucket if exists
func (b *Bucket) delete(k string) {
	if b.search(k) {
		previous := b.head
		if previous.key == k {
			b.head = previous.next
			return
		}
		for previous.next != nil {
			if previous.next.key == k {
				previous.next = previous.next.next
				return
			}
			previous = previous.next
		}
	}

}
func main() {

	hashTable := Init()
	list := []string{
		"BATO",
		"MATOKO",
		"BOMA",
		"JOHN",
		"ERIC",
		"KYLE",
		"RISE",
	}
	for _, v := range list {
		hashTable.Insert(v)
	}
	fmt.Println(hashTable)
	fmt.Println(hashTable.Search("BATO"))
	fmt.Println(hashTable.Search("ZERO"))
	hashTable.Delete("ERIC")
	fmt.Println(hashTable.Search("ERIC"))
	fmt.Println(hashTable)
	hashTable.Insert("NIKADO")
	fmt.Println(hashTable.Search("NIKADO"))
	fmt.Println(hashTable)

}
