package main

import "fmt"

// Linked list
type DoubleLinkedList struct {
	head   *Node
	last   *Node
	length int
}

// Node
type Node struct {
	previous *Node
	value    int
	next     *Node
}

// InsertFront
func (l *DoubleLinkedList) InsertFront(v int) {
	newNode := &Node{nil, v, nil}
	//If first element
	if l.head == nil {
		l.head = newNode
		l.last = newNode
		l.length += 1
	} else {
		l.InsertBeforeNode(l.head, newNode)
		l.length += 1
	}
}

// InsertBack
func (l *DoubleLinkedList) InsertBack(v int) {
	if l.last == nil {
		l.InsertFront(v)
	} else {
		newNode := &Node{nil, v, nil}
		l.InsertAferNode(l.last, newNode)
		l.length += 1
	}
}

// Remove
func (l *DoubleLinkedList) Remove(v int) {
	if l.head.value == v {
		l.head = l.head.next
		l.length -= 1
		return
	}
	node := l.head
	for node.value != v {
		if node.next == nil {
			fmt.Printf("%d is not in the list", v)
			return
		}
		node = node.next
	}
	node.previous.next = node.next
	node.next.previous = node.previous
	l.length -= 1
}

// InsertAfterValue
func (l *DoubleLinkedList) InsertAferNode(node *Node, newNode *Node) {
	newNode.previous = node
	//if last node
	if node.next == nil {
		newNode.next = nil
		//set newNode as last node
		l.last = newNode
	} else {
		newNode.next = node.next
		node.previous.next = newNode
	}
	node.next = newNode
}

// InsertBeforeValue
func (l *DoubleLinkedList) InsertBeforeNode(node *Node, newNode *Node) {
	newNode.next = node
	//if head
	if node.previous == nil {
		newNode.previous = nil
		//set newNode as a head
		l.head = newNode
	} else {
		newNode.previous = node.previous
		//Node before default node should now point to newNode
		node.previous.next = newNode
	}
	node.previous = newNode
}

func (l DoubleLinkedList) printLinkedList() {
	toPrint := l.head
	for l.length != 0 {
		fmt.Printf("%d ", toPrint.value)
		toPrint = toPrint.next
		l.length -= 1
	}
}

func main() {
	myList := []int{1, 2, 3, 4}
	myLinkedList := DoubleLinkedList{}
	for _, v := range myList {
		myLinkedList.InsertFront(v)
	}
	myLinkedList.printLinkedList()
	myLinkedList.Remove(3)
	fmt.Println()
	myLinkedList.printLinkedList()
	fmt.Println()
	myLinkedList.InsertBack(5)
	fmt.Println(myLinkedList.last.value)
	myLinkedList.printLinkedList()
}
