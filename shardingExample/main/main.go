package main

import "fmt"

type HashTable struct {
	arr []*Bucket
}

type Bucket struct {
	head *BucketNode
}

type BucketNode struct {
	key  int
	next *BucketNode
}

func Init(size int) *HashTable {
	arr := make([]*Bucket, size)
	result := &HashTable{arr: arr}
	for i := 0; i < size; i++ {
		result.arr[i] = &Bucket{}
	}
	return result
}

func (t *HashTable) hash(key int) int {
	index := key % len(t.arr)
	return index
}

func (t *HashTable) Insert(key int) int {
	index := t.hash(key)
	t.arr[index].insert(key)
	return index
}

func (t *HashTable) Search(key int) bool {
	index := t.hash(key)
	return t.arr[index].search(key)
}

func (b *Bucket) insert(key int) {
	if !b.search(key) {
		newNode := &BucketNode{key, &BucketNode{}} //pointer to new node instance
		previous := b.head                         //pointer to bucket head
		b.head = newNode                           //set bucket head to new instance
		newNode.next = previous                    //set next of current b.head (new instance) to old b.head
	} else {
		fmt.Printf("%d is already exists\n", key)
	}

}

func (b *Bucket) search(key int) bool {
	currentNode := b.head
	for currentNode != nil {
		if currentNode.key == key {
			return true
		}
		currentNode = currentNode.next
	}
	return false
}

func (t *HashTable) Delete(key int) {
	index := t.hash(key)
	t.arr[index].delete(key)
}

func (b *Bucket) delete(key int) {
	previous := b.head
	if previous.key == key {
		b.head = previous.next
		return
	}
	for previous.next != nil {
		if previous.next.key == key {
			previous.next = previous.next.next
		}
		previous = previous.next
		return
	}
	fmt.Printf("%d not found!\n", key)
}

func main() {

	//N := 400000
	//
	//split := make([]map[int]int, 200)
	//
	//for i := range split {
	//	split[i] = make(map[int]int)
	//}
	//for i := 0; i < N; i++ {
	//	value := int(i)
	//	split[i%200][value] = value
	//}
	//runtime.GC()
	//_ = split[0][0]
	//var x []int
	//fmt.Println(len(x), cap(x))
	//x = append(x,10)
	//fmt.Println(len(x), cap(x))
	//for i := 0;i<10;i++{
	//	x = append(x,i)
	//	fmt.Println(len(x), cap(x))
	//}

	test := []int{1, 3, 5, 8, 10, 13, 23}

	h := Init(len(test))

	for i := range test {
		k := test[i]
		ind := h.hash(k)
		h.Insert(k)
		fmt.Println(ind, h.arr[ind].head.key)
	}

	//fmt.Println(len(h.arr), cap(h.arr))
	//
	fmt.Println(*h)
	//
	//fmt.Println(h.Search(1))
	//fmt.Println(h.Search(3))
	fmt.Println(h.Search(1))
	//h.Delete(13)
	//fmt.Println(h.Search(13))
	//
	//fmt.Println(len(h.arr), cap(h.arr))
}
