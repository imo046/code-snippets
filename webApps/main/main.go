package main

import (
	"bufio"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
)

type Guestbook struct {
	SignsCount int
	Signs      []string
}

func readFromFile(p string) []string {
	var lines []string
	f, err := os.Open(p)
	checkErr(err)
	defer f.Close()
	s := bufio.NewScanner(f)
	for s.Scan() {
		lines = append(lines, s.Text())
	}
	return lines

}

func writeToFile(p string, sign interface{}) {
	options := os.O_WRONLY | os.O_APPEND | os.O_CREATE //params for file open
	f, err := os.OpenFile(p, options, os.FileMode(0600))
	defer func() {
		err = f.Close()
		checkErr(err)
	}()
	checkErr(err)
	_, err = fmt.Fprintln(f, sign) //add line to file
	checkErr(err)
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func viewHandlerGuestbook(writer http.ResponseWriter, request *http.Request) {
	signs := readFromFile("/media/igor/65d5f130-cee4-4463-b961-b674941218ee/igor/go/src/littleBookGo/webApps/main/signs.txt")
	guestbook := &Guestbook{SignsCount: len(signs), Signs: signs}
	fmt.Printf("%#v\n", signs)
	html, err := template.ParseFiles("/media/igor/65d5f130-cee4-4463-b961-b674941218ee/igor/go/src/littleBookGo/webApps/main/guestbook.html")
	checkErr(err)
	err = html.Execute(writer, guestbook) //write template to response (possible to use os.Stdout instead to write to console)
	checkErr(err)
}

func formHandler(writer http.ResponseWriter, request *http.Request) {
	html, err := template.ParseFiles("/media/igor/65d5f130-cee4-4463-b961-b674941218ee/igor/go/src/littleBookGo/webApps/main/form.html")
	checkErr(err)
	err = html.Execute(writer, nil)
	checkErr(err)
}

func testHandler(message interface{}) {
	templText := "Template start\nAction: {{.}}\nEnd\n"
	t, err := template.New("test").Parse(templText)
	checkErr(err)
	err = t.Execute(os.Stdout, message) //write template to response (possible to use os.Stdout instead to write to console)
	checkErr(err)
}

func createHandler(writer http.ResponseWriter, request *http.Request) {
	sign := request.FormValue("signs")
	//_, err := writer.Write([]byte(sign))
	//checkErr(err)
	writeToFile("/media/igor/65d5f130-cee4-4463-b961-b674941218ee/igor/go/src/littleBookGo/webApps/main/signs.txt", sign)
	http.Redirect(writer, request, "/guestbook", http.StatusFound)
}

func main() {
	http.HandleFunc("/guestbook", viewHandlerGuestbook) //handle request which ends with /hello, one handler for one specific request/page
	http.HandleFunc("/guestbook/form", formHandler)
	http.HandleFunc("/guestbook/create", createHandler)
	err := http.ListenAndServe("localhost:8080", nil) //run web-server in a loop, only one is needed
	log.Fatal(err)

}
