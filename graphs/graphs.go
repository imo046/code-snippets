package graphs

import (
	"errors"
	"fmt"
)

// Graph represents an adjacency list
type Graph struct {
	vertices []*Vertex
}

// Vertex structure
type Vertex struct {
	key              int
	adjacentVertices []*Vertex
}

// Add Vertex, we add vertex to the graph
func (g *Graph) AddVertex(k int) error {
	if !contains(g.vertices, k) {
		g.vertices = append(g.vertices, &Vertex{key: k})
		return nil
	} else {
		err := errors.New(fmt.Sprintf("Vertex %d is already in the graph!", k))
		return err
	}

}

func checkIfExists(e error) {

}

// Add Edge adds an edge to the graph between two vertices
func (g *Graph) AddEdge(from, to int) {
	//get vertex using the key
	//check errors
	fromVertex, err := g.getVertex(from)
	if err != nil {
		fmt.Println(err)
		return
	}
	toVertex, err := g.getVertex(to)
	if err != nil {
		fmt.Println(err)
		return
	}
	if !contains(fromVertex.adjacentVertices, to) {
		//add edge
		fromVertex.adjacentVertices = append(fromVertex.adjacentVertices, toVertex)
	} else {
		fmt.Printf("Vertex %d --> Vertex %d already exists!\n", from, to)
	}
}

// getVertex returns a pointer to a vertex with key
func (g *Graph) getVertex(k int) (*Vertex, error) {
	for _, v := range g.vertices {
		if v.key == k {
			return v, nil
		}
	}
	return nil, errors.New(fmt.Sprintf("Vertex %d does not exist!", k))
}

// contains
func contains(s []*Vertex, k int) bool {
	for _, v := range s {
		if v.key == k {
			return true
		}
	}
	return false
}

// printVertexKey
func (g *Graph) PrintVertex() {
	for _, v := range g.vertices {
		fmt.Printf("Vertex %d\n", v.key)
		for _, v := range v.adjacentVertices {
			fmt.Printf("	---> %d\n", v.key)
		}
	}

}
