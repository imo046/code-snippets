package main

import (
	"fmt"
	"littleBookGo/graphs"
	"math/rand"
	"strings"
	"time"
)

func main() {

	test := &graphs.Graph{}

	for i := 0; i < 5; i++ {
		test.AddVertex(i)
	}

	test.AddEdge(1, 2)
	test.AddEdge(6, 2)
	test.AddEdge(1, 2)
	test.AddEdge(2, 3)
	test.AddEdge(2, 4)
	test.PrintVertex()

	rand.Seed(time.Now().UnixNano())
	digits := "0123456789"
	specials := "~=+%^*/()[]{}/!@#$?|"
	all := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		digits + specials)

	length := 12
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(all[rand.Intn(len(all))])
	}
	str := b.String()
	fmt.Println(str)

}
