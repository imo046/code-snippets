package tests

import (
	"littleBookGo/unitTests/src"
	"testing"
)

func TestWIthTwoElems(t *testing.T) {
	list := []string{"one", "two"}
	res := src.JoinWithComma(list)
	assertion := "one and two"
	if res != assertion {
		t.Errorf("Result '%s' didn't match expected value '%s'", res, assertion)
	}
}

func TestWIthOneElem(t *testing.T) {
	list := []string{"one"}
	res := src.JoinWithComma(list)
	assertion := "one"
	if res != assertion {
		t.Errorf("Result '%s' didn't match expected value '%s'", res, assertion)
	}
}

func TestWIthThreeElems(t *testing.T) {
	list := []string{"one", "two", "three"}
	if src.JoinWithComma(list) != "one, two, and three" {
		t.Error("Didn't match expected value")
	}
}

func TestWIthNoElems(t *testing.T) {
	list := []string{}
	res := src.JoinWithComma(list)
	assertion := ""
	if res != assertion {
		t.Errorf("Result '%s' didn't match expected value '%s'", res, assertion)
	}
}

type testData struct {
	input     []string
	assertion string
}

func TestAllVariants(t *testing.T) {
	testDataList := []testData{
		{[]string{"one"}, "one"},
		{[]string{"one", "two"}, "one and two"},
		{[]string{"one", "two", "three"}, "one, two, and three"},
	}
	for _, val := range testDataList {
		res := src.JoinWithComma(val.input)
		if res != val.assertion {
			t.Errorf("Result '%s' didn't match expected value '%s'", res, val.assertion)
		}
	}
}
