package src

import "strings"

func JoinWithComma(words []string) string {
	if len(words) < 3 {
		return strings.Join(words, " and ")
	}
	res := strings.Join(words[:len(words)-1], ", ")
	res += ", and "
	res += words[len(words)-1]
	return res
}
