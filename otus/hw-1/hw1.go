package hw_1

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

//import "unicode/utf8"
//import "strings"

func TestRun() {
	type myAge uint32
	//a := 4
	//b := myAge(a)
	for i := 0; i < 5; i++ { //outer loop
		fmt.Println("-")
		for j := 0; j < i; j++ { //inner loop
			fmt.Print("*")
		}
	}
}

func UnpackString(s string) string {
	var sb strings.Builder
	for i, rune := range s {
		if i > 0 && unicode.IsNumber(rune) {
			mystr := strings.Repeat(string(s[i-1]), int(rune))
			sb.WriteString(mystr)
			//sb.WriteRune(rune)
		}
	}
	return sb.String()
}
func isLetter(s string) bool {
	for _, r := range s {
		if !unicode.IsLetter(r) {
			return false
		}
	}
	return true
}

func TestIfNum(s string) string {
	var sb strings.Builder
	for i, rune := range s {
		if i > 0 && unicode.IsNumber(rune) {
			runeToInt, err := strconv.Atoi(string(rune))
			if err == nil {
				mystr := strings.Repeat(string(s[i-1]), runeToInt)
				fmt.Println(mystr)
				sb.WriteString(mystr)
			}
		}
	}
	return sb.String()
}
