package main

import (
	"fmt"
	hw_2 "littleBookGo/otus/hw-2"
	"os"
)

func foo(callback func(...int) int, xi ...int) int {
	return callback(xi...)
}

func main() {
	//test := "ss3f4"
	//res := hw_1.TestIfNum(test)
	//fmt.Println(res)
	//hw_2.TestArr()
	//test2, test3 := []string{"a","b"}, []string{"c"}
	//test4 := hw_2.Concat(test2,test3)
	//fmt.Println(test4)
	//fmt.Println(os.Getwd())
	rootPath, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	filePath := rootPath + "/otus" + "/hw-2" + "/test.txt"
	//
	t := hw_2.ReadFrom(filePath)
	test5 := hw_2.CalculateWords(t, 10)
	//
	for _, v := range test5 {
		fmt.Println(v)
	}
	//http.HandleFunc("/hello",timed(hello))
	//http.ListenAndServe(":3000",nil)
}

//func timed(f func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
//	return func(w http.ResponseWriter, r *http.Request) {
//		start := time.Now()
//		f(w,r)
//		end := time.Now()
//		fmt.Println("Request took", end.Sub(start))
//	}
//}
//
//func hello(w http.ResponseWriter, r *http.Request) {
//	fmt.Fprintln(w,"<h1>Hello!</h1>")
//}
