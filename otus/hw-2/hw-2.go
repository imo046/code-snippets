package hw_2

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

func TestArr() {
	s := make([]int, 2, 4)
	fmt.Println(s)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func Concat(slices ...[]string) []string {
	var res []string
	for _, s := range slices {
		res = append(res, s...)
	}
	return res
}

func ReadFrom(p string) string {
	data, err := ioutil.ReadFile(p)
	check(err)
	return string(data)
}

type WordCount struct {
	word  string
	count int
}

func removeNewLines(s string) string {
	strs := strings.Split(s, "\n")
	res := strings.Join(strs, " ")
	return res
}

type ByCount []*WordCount

func (a ByCount) Len() int           { return len(a) }
func (a ByCount) Less(i, j int) bool { return a[i].count < a[j].count }
func (a ByCount) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func CalculateWords(text string, l int) []*WordCount {
	var countList []*WordCount
	trimmed := map[string]bool{
		":": true,
		".": true,
		",": true,
		"`": true,
	}
	keyWords := make(map[string]int)
	for _, s := range strings.Split(removeNewLines(text), " ") {
		w := strings.TrimFunc(strings.TrimSpace(s), func(r rune) bool {
			if trimmed[string(r)] {
				return true
			} else {
				return false
			}
		})
		keyWords[w]++
		//if _, value := keyWords[w]; !value {
		//	keyWords[w] = 1
		//} else {
		//	keyWords[w] += 1
		//}
	}
	for k, v := range keyWords {
		countList = append(countList, &WordCount{word: k, count: v})
	}
	sort.Sort(ByCount(countList))
	return countList[len(countList)-l:]
}
