package main

import (
	"fmt"
	"time"
)

//channels and routines
//channels work as queues with FIFO
//channels can be one-direction (only writing or reading) and two-direction, bufferized and nonbufferized, closed and open
//create channel ch = make(chan int) or make(chan int, 3) with capacity
//send to channel: ch <- value,
//get from channel: x := <- ch or x,ok := <- ch, if ok == true -> channel is open
//close channel: close(ch)
//Reading from the closed channel returns 0
//Writing to closed channel results in panic()
//if multiple writers, the one which creates a channel should close it
//read-only channel: f(in <-chan int)
//write-only channel: f(out chan<- int)
//buffered channel blocked only after capacity is full
//reading from buffered channel blocked only if it's empty

// writer
func fin(inCh chan<- int, val int) {
	inCh <- val
}

// reader
func fout(outCh <-chan int) {
	fmt.Printf("%d\n", <-outCh)
}

// writer in buffered
func finBuff(inCh chan<- int, vals ...int) {
	for _, v := range vals {
		fmt.Printf("Writing %d", v)
		inCh <- v
	}
}

// reader in buffered
func foutBuff(outCh <-chan int) {
	for x := range outCh {
		fmt.Printf("%d\n", x)
	}
}

// examples graceful shutdown
func serv1() error {
	for {
	} //empty cycle
}

func serv2() error {
	for {
	}
}

func main() {

	//ch := make(chan struct{})
	//chRW := make(chan int)
	chBuff := make(chan int, 3)
	//chStart := make(chan struct{})
	//go fin(chBuff,5)
	//go fout(chBuff)
	for i := 0; i < 3; i++ {
		go fin(chBuff, i+1)
		fmt.Println("Writing!")
	}

	go foutBuff(chBuff)
	time.Sleep(3 * time.Second) //important!
	close(chBuff)
	//go func() {
	//	fmt.Println("Hello")
	//	close(ch) //if closed, ok == false
	//}()
	//_,ok := <- ch //Read from channel
	//fmt.Println(ok)

	//for i := 0; i < 3; i++{
	//	chBuff <- i
	//	fmt.Println("Writing!")
	//}
	//fmt.Printf("len: %d, cap: %d\n", len(chBuff),cap(chBuff))

	//for i := 0; i < 10; i ++ {
	//	go func() {
	//		<- chStart //goroutines won't start until all 10 are created
	//	}()
	//}
	//close(chStart)

	////Timer will be executed after period of time
	//    timer := time.NewTimer(10 * time.Second)
	//
	//    select {
	//    case data := <- chStart:
	//    	fmt.Printf("recieved %v\n", data)
	//    case <-timer.C:
	//    	fmt.Printf("failed to recieve data in 10 sec")
	//	}
	//

	////Ticker will be executed once per time period
	//    ticker := time.NewTicker(5 * time.Second)
	//    for {
	//		select {
	//    	case <-ticker.C:
	//    		fmt.Printf("do something")
	//		}
	//	}

	//Graceful shutdown
	//    interrupt := make(chan os.Signal, 1)
	//    signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)//channel we listen to, signals we wait
	//
	//    fmt.Printf("Got %v...\n", <-interrupt)
	//    var chErr = make(chan error,2)
	//    go func() {
	//    	chErr <- serv1()
	//	}()
	//    go func() {
	//    	chErr <- serv2()
	//	}()
	//    for err := range chErr{
	//    	fmt.Println(err)
	//	}
	//will work until error appears, in case of error we can do something

}
