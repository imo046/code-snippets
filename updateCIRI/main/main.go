package main

import (
	"encoding/csv"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

type Oid struct {
	Oid string `json:"$oid"`
}

type MainId struct {
	Oid Oid
}

type AttrData struct {
	Val  string      `json:"val"`
	Iri  interface{} `json:"iri"`
	Meta interface{} `json:"meta"`
	Num  int         `json:"num"`
	Err  interface{} `json:"err"`
}

type Id struct {
	Obj []AttrData `json:"obj"`
}

type Organism struct {
	Obj []AttrData `json:"obj"`
}

type MyAttr struct {
	Organism Organism `json:"tax:organism"`
}

type Obj struct {
	Attrs []MyAttr    `json:"attrs"`
	Valid interface{} `json:"valid"`
	Cur   interface{} `json:"cur"`
	Id    string      `json:"id"`
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type Annotation struct {
	Message string      `json:"message"`
	Loc     interface{} `json:"loc"`
}

type Entry struct {
	Id          MainId       `json:"_id"`
	Stage       string       `json:"stage"`
	Source      string       `json:"source"`
	Obj         Obj          `json:"obj"`
	Annotations []Annotation `json:"annotations"`
}

func extractValues(data Obj, myMap map[string]string) {
	for _, elem := range data.Attrs {
		if len(elem.Organism.Obj) > 0 {
			myMap[data.Id] = elem.Organism.Obj[0].Val
		}
	}

}

func writeToCSV(myMap map[string]string, file *os.File) {
	writer := csv.NewWriter(file)
	for key, value := range myMap {
		r := make([]string, 0, 2)
		r = append(r, key)
		r = append(r, value)
		err := writer.Write(r)
		checkErr(err)
	}
	writer.Flush()

}

func main() {

	jsonDataPath := "/home/igor/Downloads/data.json"
	filePath, createErr := os.Create("/home/igor/Downloads/data.csv")
	checkErr(createErr)
	jsonFile, openErr := os.Open(jsonDataPath)
	checkErr(openErr)
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var data []Entry
	parseErr := json.Unmarshal(byteValue, &data)
	checkErr(parseErr)
	defer jsonFile.Close()
	myMap := make(map[string]string)
	for i := 0; i < len(data); i++ {
		extractValues(data[i].Obj, myMap)
	}
	writeToCSV(myMap, filePath)
	//for key, value := range myMap {
	//	fmt.Println(key + " : " + value)
	//}

}
